# Optimal State-Feedback and Static Output-Feedback Guaranteed Cost Control of Discrete-Time Linear Systems subject to Structured Uncertainties

This repository contains the MATLAB implementation of the numerical examples for the paper "Optimal State-Feedback and Static Output-Feedback Guaranteed Cost Control of Discrete-Time Linear Systems subject to Structured Uncertainties".

## Pre-requisites

### Controller synthesis and simulation (MATLAB)

The Matlab source code in this repository requires:

- [MATLAB R2015b](http://www.mathworks.com/products/matlab/)

and

- [YALMIP Toolbox](http://users.isy.liu.se/johanl/yalmip/)
- [Mosek](https://www.mosek.com/)

Note 1: Mosek has free-licenses for Academia.
Note 2: Alternatively [SeDuMi](http://sedumi.ie.lehigh.edu/) can be used.

### Controller implementation (Python)

The python souce code in this repository requires:

- [Python 2.7](https://www.python.org/download/releases/2.7/)

and

- [NumPy](http://www.numpy.org)
- [SciPy](http://www.scipy.org)

### Installing pre-requisites

#### Yalmip

 1. Create a folder named **tbxmanager**
 2. Go to this folder in Matlab
 3. Execute `urlwrite('http://www.tbxmanager.com/tbxmanager.m', 'tbxmanager.m');`
 4. Execute `tbxmanager install yalmip sedumi`
 5. Edit/create startup.m in your Matlab startup folder and add `tbxmanager restorepath` there

*Note: Many thanks to Michal Kvasnica, Johan L�fberg and Jos Sturm for creating these wonderful tools*

#### Mosek

 1. Download bynaries for Mosek 8
 2. Request or buy license and install it
 3. Add mosek to Matlab path

## Running the examples

The simulation are self contrained. To execute them simply type

```matlab
>> example_1
>> example_2
```

## Citing
The paper is currently being written, this section will be updated once its pre-print is available.