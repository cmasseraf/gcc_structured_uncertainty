function [k, p, e] = gcc_sof_full(a, b_u, b_w, c_y, d_y_w, c_z, d_z_u, d_z_w, q, r, n)
    % gcc_sof Generates the infinite horizon Guaranteed Cost Control for a
    %         parametric uncertain linear system
    %
    %    Author: Carlos M. Massera
    %    Instituition: University of S�o Paulo

    n_x = size(a,2);
    n_u = size(b_u,2);
    n_w = size(b_w,2);
    
    n_y = size(c_y, 1);
    n_z = size(c_z, 1);

    [~, s, v] = svd([q n; n' r]);
    mask = (diag(s) >= 1e-10);

    sigma = s(mask, mask);
    cost_base = sqrt(sigma) * v(:, mask)';
    c_c = cost_base(:, 1:n_x);
    d_c_u = cost_base(:, n_x + (1:n_u));
    n_c = size(c_c, 1);

    p_inv = sdpvar(n_x, n_x);  % p_inv is x in Theorem 3.
    y = sdpvar(n_u, n_y, 'full');
    v_bar = sdpvar(n_y, n_y, 'full');
    s = sdpvar(n_x, n_x);
    e = sdpvar(1, 1);

    m = blkdiag(e * eye(n_z), eye(n_c), p_inv, p_inv, e * eye(n_w));
    
    v = sdpvar(size(m, 1), size(m, 2), 'full');
    v(end-n_x-n_w+1:end, 1:n_z+n_c+n_x) = 0;
    v_lower = v(end-n_x-n_w+1:end, end-n_x-n_w+1:end);
    
    ms_nk = blkvar;
    ms_nk(1,1) = - 0.5 * eye(n_z);
    ms_nk(2,2) = - 0.5 * eye(n_c);
    ms_nk(3,3) = - 0.5 * eye(n_x);
    ms_nk(4,4) = - 0.5 * eye(n_x);
    ms_nk(5,5) = - 0.5 * eye(n_w);
    
    ms_nk(1,4) = c_z;
    ms_nk(4,1) = 0;
    ms_nk(1,5) = d_z_w;
    ms_nk(5,1) = 0;
    
    ms_nk(2,4) = c_c;
    ms_nk(4,2) = 0;
    ms_nk(2,5) = 0;
    ms_nk(5,2) = 0;
    
    ms_nk(3,4) = a;
    ms_nk(4,3) = 0;
    ms_nk(3,5) = b_w;
    ms_nk(5,3) = 0;
    
    ms_prek = [d_z_u; 
               d_c_u; 
               b_u; 
               zeros(n_x + n_w, n_u);];
    ms_postk = [zeros(n_y, n_z + n_c + n_x), c_y, d_y_w];
    
    n = ms_nk * v - ms_prek * y * ms_postk;
    
    gcc_lmi = blkvar;
    gcc_lmi(1,1) = -m;
    gcc_lmi(1,3) = v;
    gcc_lmi(2,2) = -m;
    gcc_lmi(2,3) = n+m;
    gcc_lmi(3,3) = -v-v';
    
    cost_lmi = blkvar;
    cost_lmi(1,1) = - p_inv;
    cost_lmi(1,2) = eye(n_x);
    cost_lmi(2,2) = - s;

    constraints = [gcc_lmi <= 0;
                   cost_lmi <= 0;
                   p_inv >= 0;
                   e >= 0;
                   v_bar * [c_y d_y_w] == [c_y d_y_w] * v_lower;
                   ];

    opt = sdpsettings('solver', '+mosek', 'verbose', 0);
    sol = optimize(constraints, trace(s), opt);

    if sol.problem ~= 0
        warning('gcc:solver_failed','Solver did not converge');
    end

    k = value(y) / value(v_bar);
    p = inv(value(p_inv));
    e = value(e);

end