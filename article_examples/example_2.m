%% System model

% a = [1, 0.100000000000000, 0.00890137907196124  , -0.0136353209589634;
%      0, 1                , 0.000192745824420109 ,  0.00910712073447584;
%      0, 0                , 0.842438297028269    , -0.0236460383253255;
%      0, 0                , 0.0362646422149533   ,  0.826727460798624];
% b_u = [3.47776838200551e-05;
%        0.00227074621985734;
%        0.667680835927827;
%        0.444918673681622];
% b_w = [3.85450749530399e-07,  9.11993747821017e-05;
%        2.51673123766975e-05, -3.49610568219739e-05;
%        0.00740009254173098,   0.00756094876596724;
%        0.00493115749565128,  -0.00672509627496858];
% 
% c_y = [1, 0, 0, 0;
%        0, 1, 0, 0;
%        0, 0, 1, 0;
%        0, 0, 0, 1;
%        0, 0, 0, 0;
%        0, 0, 0, 0];
% d_y_w = [0, 0;
%          0, 0;
%          0, 0;
%          0, 0;
%          1, 0;
%          0, 1];
%    
% c_z = [0, 0, -5.97739883437123, -6.36592975860536;
%        0, 0, -7.99176899761293, 12.2673654113358];
% d_z_u = [59.7739883437123;
%           0];
% d_z_w = [0, 0;
%          0, 0];

a = [1.1 0   0;
       0 0 1.2;
      -1 1   0];
b_u = [ 0 1;
      1 1;
     -1 0];
b_w = [0.7 0.3; 0.5 -0.4; -0.7 0.1];

c_y = [eye(3,3); 
       0 0 0];
d_y_w = [zeros(3, 2); 0 1];
   
c_z = [ 0.4123    0.4318   -0.5028
       -0.0061   -0.3218    0.4441];
d_z_u = [0.4 -0.4;
         0  0];
d_z_w = zeros(2,2);

n_w = size(d_z_w, 2);

%% Cost function matrices
q = eye(3);
r = eye(2);
n = zeros(3,2);

%% Generate controlers
[k_su, p_su] = gcc_sof_diag_full(a, b_u, b_w, c_y, d_y_w, c_z, d_z_u, d_z_w, q, r, n);

%% Print traces
display(['Nominal cost for GCC with structured uncertainty (full): ' num2str(trace(p_su))])

%% Start of simulation
n_runs = 10000;
n_steps = 200;
x0_samples = randn(3, n_runs);

cost_su = 0;

for sample = 1:size(x0_samples, 2)
    x_su = x0_samples(:, sample);
    
    for i = 1:n_steps
        delta = diag(2 * rand(n_w, 1) - 1);
        
        % d_z_u * u_su is ommited since d_y_w * delta * d_z_u = 0
        w_su_eff = (eye(n_w) - delta * d_z_w) \ delta * (c_z * x_su);
        y_su = c_y * x_su + d_y_w * w_su_eff;
        u_su = - k_su * y_su;
        % now we calculate the real w_su
        w_su = (eye(n_w) - delta * d_z_w) \ delta * (c_z * x_su + d_z_u * u_su);
        
        % update the cost
        cost_su = cost_su + x_su' * q * x_su + u_su' * r * u_su;
        
        % update the state
        x_su = a * x_su + b_u * u_su + b_w * w_su;
    end
end

mean_cost_su = cost_su / n_runs

%% Second simulation, for plots
x_su = [1, 1, 1]';

X = [];
U = [];
Delta = [];

N = 20;
for i = 1:N
    delta = diag(2 * rand(n_w, 1) - 1);
    
    X = [X; x_su'];
    Delta = [Delta; diag(delta)'];

    % d_z_u * u_su is ommited since d_y_w * delta * d_z_u = 0
    w_su_eff = (eye(n_w) - delta * d_z_w) \ delta * (c_z * x_su);
    y_su = c_y * x_su + d_y_w * w_su_eff;
    u_su = - k_su * y_su;
    
    U = [U; u_su'];
    
    % now we calculate the real w_su
    w_su = (eye(n_w) - delta * d_z_w) \ delta * (c_z * x_su + d_z_u * u_su);

    % update the cost
    cost_su = cost_su + x_su' * q * x_su + u_su' * r * u_su;

    % update the state
    x_su = a * x_su + b_u * u_su + b_w * w_su;
end

