%%
columnwidth = 8.63;
textwidth = 17.78;

%%
fig = figure(1);
set(gca, 'FontSize', 10)

plot(0:N-1, Delta(:,1))
hold on
plot(0:N-1, Delta(:,2))
grid on
xlabel('Timestep - k')
ylabel('Uncertainty - \delta_i')
legend('\delta_1', '\delta_2')

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 textwidth / 3 textwidth / 3];
fig.PaperSize = [textwidth / 3 textwidth / 3];

print -dpng -r300 results/results_1.png

%%
fig = figure(2);
set(gca, 'FontSize', 10)

hold on
plot(0:N-1, X(:,1), 'b-');
plot(0:N-1, X(:,2), 'r--')
plot(0:N-1, X(:,3), 'k-.')
plot(0:N-1, -ones(size(X(:,1))), 'k--')
ylim([-1.1, 1.1])

grid on
xlabel('Timestep - k')
ylabel('State - x_k')
legend('State 1', 'State 2', 'State 3')

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 textwidth / 3 textwidth / 3];
fig.PaperSize = [textwidth / 3 textwidth / 3];

print -dpng -r300 results/results_2.png

%%
fig = figure(3);
set(gca, 'FontSize', 10)

hold on
plot(0:N-1, U(:,1), 'b-');
plot(0:N-1, U(:,2), 'r--');

grid on
xlabel('Timestep - k')
ylabel('Control Input - u_k')
legend('Input 1', 'Input 2', 'Location', 'southeast')

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 textwidth / 3 textwidth / 3];
fig.PaperSize = [textwidth / 3 textwidth / 3];

print -dpng -r300 results/results_3.png