%% System model

a = [1.1 0   0;
       0 0 1.2;
      -1 1   0];
b_u = [ 0 1;
      1 1;
     -1 0];
b_w = [0.7 0.3; 0.5 -0.4; -0.7 0.1];

c_y = eye(3);
d_y_w = zeros(3, 2);
   
c_z = [ 0.4123    0.4318   -0.5028
       -0.0061   -0.3218    0.4441];
d_z_u = [0.4 -0.4;
         0  0];
d_z_w = zeros(2,2);

n_w = size(d_z_w, 2);

%% Cost function matrices
q = eye(3);
r = eye(2);
n = zeros(3,2);

%% Generate controlers
[~, p_uu] = gcc_sof(a, b_u, b_w, c_y, d_y_w, c_z, d_z_u, d_z_w, q, r, n);
[k_uu, p_uuf] = gcc_sof_full(a, b_u, b_w, c_y, d_y_w, c_z, d_z_u, d_z_w, q, r, n);
[~, p_su] = gcc_sof_diag(a, b_u, b_w, c_y, d_y_w, c_z, d_z_u, d_z_w, q, r, n);
[k_su, p_suf] = gcc_sof_diag_full(a, b_u, b_w, c_y, d_y_w, c_z, d_z_u, d_z_w, q, r, n);
[k_lqr, p_lqr] = dlqr(a, b_u, q, r, n);

%% Print traces
display(['Nominal cost for GCC with unstructured uncertainty: ' num2str(trace(p_uu))])
display(['Nominal cost for GCC with unstructured uncertainty (full): ' num2str(trace(p_uuf))])
display(['Nominal cost for GCC with structured uncertainty: ' num2str(trace(p_su))])
display(['Nominal cost for GCC with structured uncertainty (full): ' num2str(trace(p_suf))])
display(['Nominal cost for LQR: ' num2str(trace(p_lqr))])

%% Start of simulation
n_runs = 10000;
n_steps = 200;
x0_samples = randn(3, n_runs);

cost_uu = 0;
cost_su = 0;
cost_lqr = 0;

for sample = 1:size(x0_samples, 2)
    x_uu = x0_samples(:, sample);
    x_su = x0_samples(:, sample);
    x_lqr = x0_samples(:, sample);
    
    for i = 1:n_steps
        delta = diag(2 * rand(n_w, 1) - 1);
        
        % d_z_u * u_uu is ommited since d_y_w * delta * d_z_u = 0
        w_uu_eff = (eye(n_w) - delta * d_z_w) \ delta * (c_z * x_uu);
        y_uu = c_y * x_uu + d_y_w * w_uu_eff;
        u_uu = - k_uu * y_uu;
        % now we calculate the real w_uu
        w_uu = (eye(n_w) - delta * d_z_w) \ delta * (c_z * x_uu + d_z_u * u_uu);
        
        % update the cost
        cost_uu = cost_uu + x_uu' * q * x_uu + u_uu' * r * u_uu;
        
        % update the state
        x_uu = a * x_uu + b_u * u_uu + b_w * w_uu;
        
        % d_z_u * u_su is ommited since d_y_w * delta * d_z_u = 0
        w_su_eff = (eye(n_w) - delta * d_z_w) \ delta * (c_z * x_su);
        y_su = c_y * x_su + d_y_w * w_su_eff;
        u_su = - k_su * y_su;
        % now we calculate the real w_su
        w_su = (eye(n_w) - delta * d_z_w) \ delta * (c_z * x_su + d_z_u * u_su);
        
        % update the cost
        cost_su = cost_su + x_su' * q * x_su + u_su' * r * u_su;
        
        % update the state
        x_su = a * x_su + b_u * u_su + b_w * w_su;
        
        u_lqr = - k_lqr * x_lqr;
        % now we calculate the real w_lqr
        w_lqr = (eye(n_w) - delta * d_z_w) \ delta * (c_z * x_lqr + d_z_u * u_lqr);
        
        % update the cost
        cost_lqr = cost_lqr + x_lqr' * q * x_lqr + u_lqr' * r * u_lqr;
        
        % update the state
        x_lqr = a * x_lqr + b_u * u_lqr + b_w * w_lqr;
    end
end

mean_cost_uu = cost_uu / n_runs
mean_cost_su = cost_su / n_runs
mean_cost_lqr = cost_lqr / n_runs