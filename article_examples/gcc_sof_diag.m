function [k, p, e] = gcc_sof_diag(a, b_u, b_w, c_y, d_y_w, c_z, d_z_u, d_z_w, q, r, n)
    % gcc_sof Generates the infinite horizon Guaranteed Cost Control for a
    %         parametric uncertain linear system
    %
    %    Author: Carlos M. Massera
    %    Instituition: University of S�o Paulo

    n_x = size(a,2);
    n_u = size(b_u,2);
    n_w = size(b_w,2);
    
    n_y = size(c_y, 1);
    n_z = size(c_z, 1);

    [~, s, v] = svd([q n; n' r]);
    mask = (diag(s) >= 1e-10);

    sigma = s(mask, mask);
    cost_base = sqrt(sigma) * v(:, mask)';
    c_c = cost_base(:, 1:n_x);
    d_c_u = cost_base(:, n_x + (1:n_u));
    n_c = size(c_c, 1);

    p_inv = sdpvar(n_x, n_x);
    x_bar = sdpvar(n_y, n_y);
    y = sdpvar(n_u, n_y, 'full');
    s = sdpvar(n_x, n_x);
    e = diag(sdpvar(n_w, 1));

    lmi_gcc = blkvar;
    lmi_gcc(1,1) = - e;
    lmi_gcc(1,4) = c_z * p_inv - d_z_u * y * c_y;
    lmi_gcc(1,5) = d_z_w * e;
    lmi_gcc(2,2) = - eye(n_c);
    lmi_gcc(2,4) = c_c * p_inv - d_c_u * y * c_y;
    lmi_gcc(3,3) = - p_inv;
    lmi_gcc(3,4) = a * p_inv - b_u * y * c_y;
    lmi_gcc(3,5) = b_w * e;
    lmi_gcc(4,4) = - p_inv;
    lmi_gcc(5,5) = - e;
    
    cost_lmi = blkvar;
    cost_lmi(1,1) = - p_inv;
    cost_lmi(1,2) = eye(n_x);
    cost_lmi(2,2) = - s;

    constraints = [lmi_gcc <= 0;
                   cost_lmi <= 0;
                   p_inv >= 0;
                   e >= 0;
                   x_bar * c_y == c_y * p_inv;
                   ];

    opt = sdpsettings('solver', '+mosek', 'verbose', 0);
    sol = optimize(constraints, trace(s), opt);

    if sol.problem ~= 0
        warning('gcc:solver_failed','Solver did not converge');
    end

    k = value(y) / value(x_bar);
    p = inv(value(p_inv));
    e = value(diag(e));

end